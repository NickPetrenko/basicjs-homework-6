// 1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної 
// ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.

let product = {
    name: "Футболка",
    price: 100,
    discount: 20,
    calculateTotalPrice: function() {
      let discountedPrice = this.price - (this.price * (this.discount / 100));
      return discountedPrice;
    }
  };
  
  let totalPrice = product.calculateTotalPrice();
  console.log("Повна ціна товару: " + totalPrice);




// 2. Напишіть функцію greeting, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
// наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік за допомогою prompt, і викличте функцію 
// gteeting з введеними даними(передавши їх як аргументи). Результат виклику функції виведіть з допомогою alert.

function greeting(obj) {
    let message = "Привіт, я " + obj.name + "! Мені " + obj.age + " років.";
    return message;
}
  
let name = prompt("Введіть ваше ім'я:");
let age = prompt("Введіть ваш вік:");
  
let user = {
    name: name,
    age: age
};
  
let greetingMessage = greeting(user);
alert(greetingMessage);